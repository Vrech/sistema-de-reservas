<?php
    require 'config.php';
    require 'classes/reservas.class.php';
    require 'classes/carros.class.php';

    $reservas = new Reservas($pdo);
?>
<h1>Reservas</h1>

<a href="reservar.php">Adicionar reserva</a><br/><br/>

<form method="get">
    <select name="ano">
        <?php for($q = date('Y'); $q >= 2000; $q--): ?>
            <option><?php echo $q; ?></option>
        <?php endfor; ?>
    </select>

    <select name="mes">
        <?php for($m = 1; $m <= 12; $m++): ?>
            <option><?php 
                if($m < 10){
                    echo '0'.$m;
                } else {
                    echo $m;
                }
                ?></option>
        <?php endfor; ?>
    </select>

    <input type="submit" value="Mostrar"/>
</form>
<?php
if (empty($_GET['ano'])) {
    exit;
}

    $data = $_GET['ano'].'-'.$_GET['mes'];
    $dia1 = date('w', strtotime($data.'-01'));
    $dias = date('t', strtotime($data));
    $linhas = ceil(($dia1 + $dias) / 7);
    $dia1 = -$dia1;
    $data_inicio = date('Y-m-d', strtotime($dia1.' days', strtotime($data)));
    $data_fim = date('Y-m-d', strtotime(( ($dia1 + ($linhas * 7 ) - 1) ).' days', strtotime($data)));

    $lista = $reservas->getReservas($data_inicio, $data_fim);
?>
<hr/>

<?php
    require 'calendario.php';
?>
